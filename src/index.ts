import "dotenv/config";
import express from "express";
import swaggerUi from "swagger-ui-express";
import YAML from "yamljs";
import bodyParser from "body-parser";
import {
    appStatusRoute,
    checkCardRoute,
    createUserRoute,
    getAllUsersRoute,
    getUserByIdRoute,
} from "./controllers";
import { postValidator } from "./middleware/postValidator";
import { port } from "./config";
import { checkAdminSecret, checkApiKey } from "./middleware/permission";
import { testConnection } from "./db";

testConnection();

const swaggerDocument = YAML.load("./docs/openapi.yaml");

const app = express();
app.use(bodyParser.json());

app.use("/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get("/users", checkAdminSecret, getAllUsersRoute);
app.get("/users/:id", checkAdminSecret, getUserByIdRoute);
app.post("/users", checkAdminSecret, postValidator, createUserRoute);
app.get("/status", appStatusRoute);
app.get("/card/:cardNumber", checkApiKey, checkCardRoute);

const server = app.listen(port);
console.log(`Server is listening on port ${port}`);

export { app, server };
