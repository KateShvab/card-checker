const dbConnectionString =
    (process.env.NODE_ENV === "test"
        ? process.env.TEST_POSTGRES_CONN
        : process.env.POSTGRES_CONN) || "";

const port = process.env.PORT || 3000;
const adminSecret = process.env.ADMIN_SECRET || "";

export { dbConnectionString, port, adminSecret };
