import { DateTime } from "luxon";

import axios from "axios";

const cardChecker = async (cardNumber: string) => {
    try {
        const validity = await fetchData(cardNumber, "validity");
        const state = await fetchData(cardNumber, "state");

        const luxonValidityDate = DateTime.fromISO(validity.validity_end);
        const validityDateFormated = luxonValidityDate.toFormat("dd.M.yyyy");

        return validityDateFormated + " " + state.state_description;
    } catch (error: any) {
        if (error.response?.status == 404) return null;
        else throw new Error("Card Checker Error");
    }
};

export { cardChecker };

const fetchData = async (
    cardNumber: string,

    urlProperty: "state" | "validity"
) => {
    const responce = await axios.get(
        `http://private-264465-litackaapi.apiary-mock.com/cards/${cardNumber}/${urlProperty}`,
        {
            headers: {
                Accept: "application/json",
            },
        }
    );
    return responce.data;
};
