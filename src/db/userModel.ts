import {
    ModelDefined,
    Sequelize,
    STRING,
    UUID,
    UUIDV4,
    NUMBER,
} from "sequelize";
import { sequelize } from "./";

export interface IUser {
    id: number;
    name: string | null;
    surname: string | null;
    email: string;
    api_key: string;
}

export interface IUsersCreationAttributes {
    name?: string;
    surname?: string;
    email: string;
}

const usersModel = (sequelize: Sequelize) => {
    const User: ModelDefined<IUser, IUsersCreationAttributes> =
        sequelize.define(
            "UserModel",
            {
                id: {
                    type: NUMBER,
                    primaryKey: true,
                    autoIncrement: true,
                },
                name: {
                    type: STRING,
                },
                surname: {
                    type: STRING,
                },
                email: {
                    type: STRING,
                    allowNull: false,
                    unique: true,
                },
                api_key: {
                    type: UUID,
                    defaultValue: UUIDV4,
                },
            },
            {
                tableName: "users",
                timestamps: false,
            }
        );
    return User;
};

const User = usersModel(sequelize);

export { User };
