import express from "express";
import Ajv from "ajv";

const ajv = new Ajv();

const schema = {
    type: "object",
    properties: {
        email: { type: "string" },
        name: { type: "string" },
        surname: { type: "string" },
    },
    required: ["email"],
    additionalProperties: false,
};

const postValidator = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) => {
    const validate = ajv.compile(schema);
    const valid = validate(req.body);

    if (valid) next();
    else {
        res.status(400).json(validate.errors);
    }
};

export { postValidator };
