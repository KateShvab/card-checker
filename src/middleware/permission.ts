import express from "express";
import { adminSecret } from "../config";
import { User } from "../db/userModel";

const checkApiKey = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) => {
    const apikey = req.headers.apikey;

    if (!apikey || apikey.length != 36) {
        res.status(401).send("API key is missing or invalid");
        return;
    }

    const findedObj = await User.findOne({ where: { api_key: apikey } });

    if (findedObj) {
        next();
    } else {
        res.status(401).send("API key is missing or invalid");
    }
};

const checkAdminSecret = async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction
) => {
    if (req.headers.secret == adminSecret) {
        next();
    } else {
        res.status(401).send("Admin secret is missing or invalid");
    }
};

export { checkApiKey, checkAdminSecret };
