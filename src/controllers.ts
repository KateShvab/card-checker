import express from "express";
import { IUsersCreationAttributes, User } from "./db/userModel";
import { port } from "./config";
import { cardChecker } from "./cardChecker";

const getAllUsersRoute = async (
    req: express.Request,
    res: express.Response
) => {
    const usersArr = await User.findAll({ raw: true });

    if (!usersArr?.length) res.status(404).send("Not found");
    else res.json(usersArr);
};

const getUserByIdRoute = async (
    req: express.Request,
    res: express.Response
) => {
    const id = req.params.id;

    const findedObj = await User.findByPk(id);

    if (!findedObj) res.status(404).send("Not found");
    else res.json(findedObj);
};

const createUserRoute = async (req: express.Request, res: express.Response) => {
    const newUser: IUsersCreationAttributes = {
        email: req.body.email,
        name: req.body.name,
        surname: req.body.surname,
    };

    try {
        const result = await User.create(newUser);
        res.json(result);
    } catch (error: any) {
        if (error.name == "SequelizeUniqueConstraintError") {
            res.status(409).send("User already exist");
        } else {
            res.status(500);
        }
    }
};

const appStatusRoute = (req: express.Request, res: express.Response) => {
    res.send(`Server is up and listening on port ${port}`);
};

const checkCardRoute = async (req: express.Request, res: express.Response) => {
    const cardNumber = req.params.cardNumber;
    try {
        const cardCheckerResult = await cardChecker(cardNumber);
        if (cardCheckerResult) res.send(cardCheckerResult);
        else res.status(404).send("Card not found");
    } catch (error) {
        res.status(500).send("Server error");
    }
};

export {
    getAllUsersRoute,
    getUserByIdRoute,
    createUserRoute,
    appStatusRoute,
    checkCardRoute,
};
