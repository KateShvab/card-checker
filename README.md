# Litachka Card Checker

Running project url: http://38.242.131.87:3003/status
Online swagger doc: http://38.242.131.87:3003/api-docs

## Getting started

### Installation

_Below is an example of how you can install and set up the app._

1. Clone the repo
    ```sh
    git clone https://gitlab.com/KateShvab/card-checker.git
    ```
2. Install NPM packages
    ```sh
    yarn install
    ```
3. Set up the `.env` file:
    ```sh
         POSTGRES_CONN=
         TEST_POSTGRES_CONN=
         PORT=
         ADMIN_SECRET=
    ```
4. To start production version:
    ```sh
    yarn start
    ```
5. To run tests:
    ```sh
    yarn test
    ```

### Usage

_Secret for user routes is `oict`_

_Api key for check card route example: `c9f79003-9ea7-49a6-967a-7670135f6ba4`_

Use open api documentation: https://gitlab.com/KateShvab/card-checker/-/blob/main/docs/openapi.yaml
