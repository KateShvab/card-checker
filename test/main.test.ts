import { expect } from "chai";
import { agent as request } from "supertest";
import { cardChecker } from "../src/cardChecker";
import { app, server } from "../src";
//import { sequelize } from "../src/db";
import { User } from "../src/db/userModel";

let apikey: string;
let userId: number;

describe("User model", () => {
    it("Should prepare test db", async () => {
        await User.truncate();
        const allUsers = await User.findAll({ raw: true });

        expect(allUsers).to.be.instanceof(Array);
        expect(allUsers).to.have.length(0);
    }).timeout(10000);

    it("Should create new user", async () => {
        const newUser = await User.create({ email: "abcd@com" });

        apikey = newUser.dataValues.api_key;
        userId = newUser.dataValues.id;

        expect(newUser).to.be.an("object");
        expect(newUser.dataValues.email).to.equal("abcd@com");
    }).timeout(10000);

    it("Should get one user", async () => {
        const findedObj = await User.findByPk(userId);

        expect(findedObj).to.be.an("object");
        expect(findedObj?.dataValues.email).to.equal("abcd@com");
        expect(findedObj?.dataValues.id).to.equal(userId);
    }).timeout(10000);
});

describe("Card checker function", () => {
    it("Should properly return card status", async () => {
        const checkResult = await cardChecker("151654");
        expect(checkResult).to.be.a("string");
    }).timeout(10000);
});

describe("Application status route", () => {
    it("Should GET 200 status", async () => {
        let res = await request(app).get("/status");
        expect(res.status).to.equal(200);
    }).timeout(10000);
});

describe("Check card route", () => {
    it("Should GET 200 status", async () => {
        let res = await request(app).get("/card/15651").set("apikey", apikey);
        expect(res.status).to.equal(200);
    }).timeout(10000);

    it("Should GET 401 status", async () => {
        let res = await request(app)
            .get("/card/15651")
            .set("apikey", "wrong-api-key");
        expect(res.status).to.equal(401);
        expect(res.text).to.equal("API key is missing or invalid");
    }).timeout(10000);
});

server.close();
