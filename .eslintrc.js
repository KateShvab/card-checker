module.exports = {
    env: {
        browser: true,
        commonjs: true,
        es2021: true,
    },
    extends: "",
    overrides: [],
    parserOptions: {
        ecmaVersion: "latest",
    },
    rules: { quotes: ["error", "double"], "max-len": ["error", { code: 120 }] },
};
